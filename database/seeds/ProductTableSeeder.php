<?php

use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Seeder;
use App\Models\ProductVariation;
use App\Models\ProductVariationType;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $product = factory(Product::class)->create();

        // $product->categories()->save(
        //     factory(Category::class)->create()
        // );

        $product = new Product;
        $product->name = 'Coffee';
        $product->slug = 'coffee';
        $product->price = 1000;
        $product->save();

        $product->categories()->save(
            factory(Category::class)->create([
                'name' => 'Coffee',
                'slug' => 'coffee',
                'order' => 1000
            ])
        );

        $productVariationType = new ProductVariationType;
        $productVariationType->name = 'Whole bean';
        $productVariationType->save();

        $productVariationType2 = new ProductVariationType;
        $productVariationType2->name = 'Ground';
        $productVariationType2->save();

        $productVariation = new ProductVariation;
        $productVariation->product_id = $product->id;
        $productVariation->name = '250g';
        $productVariation->order = 1;
        $productVariation->product_variation_type_id = $productVariationType->id;
        $productVariation->save();

        $productVariation2 = new ProductVariation;
        $productVariation2->product_id = $product->id;
        $productVariation2->name = '500g';
        $productVariation2->order = 2;
        $productVariation2->product_variation_type_id = $productVariationType->id;
        $productVariation2->save();

        $productVariation3 = new ProductVariation;
        $productVariation3->product_id = $product->id;
        $productVariation3->name = '1kg';
        $productVariation3->order = 3;
        $productVariation3->product_variation_type_id = $productVariationType->id;
        $productVariation3->save();

        $productVariation4 = new ProductVariation;
        $productVariation4->product_id = $product->id;
        $productVariation4->name = '250g';
        $productVariation4->order = 1;
        $productVariation4->product_variation_type_id = $productVariationType2->id;
        $productVariation4->save();

        $productVariation5 = new ProductVariation;
        $productVariation5->product_id = $product->id;
        $productVariation5->name = '500g';
        $productVariation5->order = 2;
        $productVariation5->product_variation_type_id = $productVariationType2->id;
        $productVariation5->save();

        $productVariation6 = new ProductVariation;
        $productVariation6->product_id = $product->id;
        $productVariation6->name = '1kg';
        $productVariation6->order = 3;
        $productVariation6->product_variation_type_id = $productVariationType2->id;
        $productVariation6->save();
    }
}
