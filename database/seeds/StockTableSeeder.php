<?php

use App\Models\Stock;
use Illuminate\Database\Seeder;

class StockTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 6; $i++) {
            $stock = new Stock;
            $stock->product_variation_id = $i;
            $stock->quantity = rand(10, 100);
            $stock->save();
        }
    }
}
