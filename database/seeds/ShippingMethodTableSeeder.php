<?php

use App\Models\ShippingMethod;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShippingMethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $method = new ShippingMethod;
        $method->name = 'Royal mail 1st class';
        $method->price = 500;
        $method->save();

        $method2 = new ShippingMethod;
        $method2->name = 'Royal mail 2nd class';
        $method2->price = 750;
        $method2->save();

        // TODO
        // insert to country_shipping_method
        // DB::statement("INSERT INTO country_shipping_method VALUES()");
    }
}
