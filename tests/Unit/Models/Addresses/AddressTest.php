<?php

namespace Tests\Unit\Models\Addresses;

use Tests\TestCase;
use App\Models\User;
use App\Models\Address;
use App\Models\Country;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddressTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_one_country()
    {
        $address = factory(Address::class)->create([
            'user_id' => factory(User::class)->create()->id
        ]);

        $this->assertInstanceOf(Country::class, $address->country);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $address = factory(Address::class)->create([
            'user_id' => factory(User::class)->create()->id
        ]);

        $this->assertInstanceOf(User::class, $address->user);
    }

    /** @test */
    public function it_sets_old_addresses_to_not_default_when_creating_new_default_address()
    {
        $user = factory(User::class)->create();

        $oldAddress = factory(Address::class)->create([
            'user_id' => $user->id,
            'default' => true
        ]);

        $this->assertTrue((bool) $oldAddress->fresh()->default);

        $newAddress = factory(Address::class)->create([
            'user_id' => $user->id,
            'default' => true
        ]);

        $this->assertFalse((bool) $oldAddress->fresh()->default);
        $this->assertTrue((bool) $newAddress->fresh()->default);
    }
}
