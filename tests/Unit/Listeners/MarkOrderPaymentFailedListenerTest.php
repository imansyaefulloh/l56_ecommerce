<?php

namespace Tests\Unit\Listeners;

use App\Cart\Cart;
use Tests\TestCase;
use App\Models\User;
use App\Models\Order;
use App\Events\Order\OrderPaymentFailed;
use Illuminate\Foundation\Testing\WithFaker;
use App\Listeners\Order\MarkOrderPaymentFailed;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MarkOrderPaymentFailedListenerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_marks_order_as_payment_failed()
    {
        $event = new OrderPaymentFailed(
            $order = factory(Order::class)->create([
                'user_id' => factory(User::class)->create()
            ])
        );

        $listener = new MarkOrderPaymentFailed();
        $listener->handle($event);

        $this->assertEquals($order->fresh()->status, Order::PAYMENT_FAILED);
    }
}
