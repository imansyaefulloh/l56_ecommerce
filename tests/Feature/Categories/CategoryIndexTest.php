<?php

namespace Tests\Feature\Categories;

use Tests\TestCase;
use App\Models\Category;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryIndexTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function it_returna_a_collection_of_categories()
    {
        $categories = factory(Category::class, 2)->create();

        $response = $this->json('GET', '/api/categories');

        $categories->each(function ($category) use ($response) {
            $response->assertJsonFragment([
                'name' => $category->name,
                'slug' => $category->slug
            ]);
        });
    }

    /** @test */
    public function it_returna_only_parents_caetegories()
    {
        $category = factory(Category::class)->create();

        $category->children()->save(
            factory(Category::class)->create()
        );

        $this->json('GET', '/api/categories')
            ->assertJsonCount(1, 'data');
    }

    /** @test */
    public function it_returna_caetegories_ordered_by_their_given_order()
    {
        $category = factory(Category::class)->create(['order' => 2]);
        $anotherCategory = factory(Category::class)->create(['order' => 1]);

        $this->json('GET', '/api/categories')
            ->assertSeeInOrder([
                $anotherCategory->slug,
                $category->slug
            ]);
    }
}
