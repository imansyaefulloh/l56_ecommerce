<?php

namespace Tests\Feature\Addressess;

use Tests\TestCase;
use App\Models\User;
use App\Models\Country;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddressStoreTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function it_fails_if_not_authenticated()
    {
        $this->json('POST', 'api/addresses')
            ->assertStatus(401);
    }

    /** @test */
    public function it_requires_a_name()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/addresses')
            ->assertJsonValidationErrors(['name']);
    }

    /** @test */
    public function it_requires_address_line_one()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/addresses')
            ->assertJsonValidationErrors(['address_1']);
    }

    /** @test */
    public function it_requires_a_city()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/addresses')
            ->assertJsonValidationErrors(['city']);
    }

    /** @test */
    public function it_requires_postal_code()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/addresses')
            ->assertJsonValidationErrors(['postal_code']);
    }

    /** @test */
    public function it_requires_a_country()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/addresses')
            ->assertJsonValidationErrors(['country_id']);
    }

    /** @test */
    public function it_requires_a_valid_country()
    {
        $user = factory(User::class)->create();

        $res = $this->jsonAs($user, 'POST', 'api/addresses', [
            'country_id' => 1
        ])->assertJsonValidationErrors(['country_id']);
    }

    /** @test */
    public function it_stores_an_address()
    {
        $user = factory(User::class)->create();

        $this->jsonAs($user, 'POST', 'api/addresses', $payload = [
            'name' => 'Iman Syaefulloh',
            'address_1' => 'Jl Asal no 103',
            'city' => 'Bandung',
            'postal_code' => '40617',
            'country_id' => factory(Country::class)->create()->id
        ]);

        $this->assertDatabaseHas('addresses', array_merge($payload, [
            'user_id' => $user->id
        ]));
    }

    /** @test */
    public function it_returns_an_address_when_created()
    {
        $user = factory(User::class)->create();

        $response = $this->jsonAs($user, 'POST', 'api/addresses', $payload = [
            'name' => 'Iman Syaefulloh',
            'address_1' => 'Jl Asal no 103',
            'city' => 'Bandung',
            'postal_code' => '40617',
            'country_id' => factory(Country::class)->create()->id
        ]);

        $response->assertJsonFragment([
            'id' => json_decode($response->getContent())->data->id
        ]);
    }
}
